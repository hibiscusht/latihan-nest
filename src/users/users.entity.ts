/* eslint-disable prettier/prettier */
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  Name: string; 

  @Column()
  Username: string; 

  @Column()
  Password: string;
  
  @Column()
  createdAt: string; 

  @Column()
  updatedAt: string; 
}