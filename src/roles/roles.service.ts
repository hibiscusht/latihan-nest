import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Roles } from './roles.entity';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Roles)
    private rolesRepository: Repository<Roles>,
  ) {}

  async addData(data: any) {
    const dt =  this.rolesRepository.create(data)
    await this.rolesRepository.save(dt)
  }

  findAll(): Promise<Roles[]> {
    return this.rolesRepository.find();
  }

  findOne(id: number): Promise<Roles | null> {
    return this.rolesRepository.findOneBy({ id });
  }

  async remove(id: number): Promise<void> {
    await this.rolesRepository.delete(id);
  }
}