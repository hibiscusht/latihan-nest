/* eslint-disable prettier/prettier */
import { Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from './auth.guard'; 
import { UsersService } from './users/users.service';
import { RolesService } from './roles/roles.service';
import { Request } from 'express';
import { JwtGuard } from './jwt.guard';
import { userToken } from './roles/roles.decorator';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, /* private readonly usersService: UsersService, */ private readonly rolesService: RolesService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('auth')
 // @UseGuards(AuthGuard)
  findAll(): any {
     return this.rolesService.findAll()
  }

  @Get('roles/add')
  // @UseGuards(AuthGuard)
   addRole(@Req() request: Request): any {
      return this.rolesService.addData({role_type: `${request.query.role_type}`})
   }

/*  @Get('user')
  getUser(): any {
    return this.usersService.findAll()
  }

  @Post('user/add')
  addUser(@Req() request: Request) {
     const flat = JSON.parse(JSON.stringify(request.body))
     return {status: "ok", data: flat}
  } */

  @Get('guards')
  @UseGuards(JwtGuard,AuthGuard) 
  useGuard(@Req() request: any): any {
    return `ini ${request.token} dan ini ${request.query.role}`
  }
}
