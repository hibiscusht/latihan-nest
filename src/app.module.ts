/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm'
import { Roles } from './roles/roles.entity' 
import { Users } from './users/users.entity';
import { RolesService } from './roles/roles.service';
import { UsersService } from './users/users.service';
import { RolesModule } from './roles/roles.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [ 
    RolesModule,
    UsersModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      url: 'postgres://default:GL3xVDPoOAf6@ep-spring-rice-43372228-pooler.us-east-1.postgres.vercel-storage.com:5432/verceldb',
      password: 'GL3xVDPoOAf6',
      ssl: {
        rejectUnauthorized: false,
      },
      entities: [Roles],
      synchronize: false,
    })],
  controllers: [AppController],
  providers: [AppService,RolesService],
})
export class AppModule {}
