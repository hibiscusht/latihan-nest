/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common'
import { Observable } from 'rxjs'
import { RolesService } from './roles/roles.service' 

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private roleService: RolesService){

  } 
  async canActivate(
    context: ExecutionContext,
  ):  Promise<boolean>  {
    const request = context.switchToHttp().getRequest()
    const role = await this.roleService.findAll()
    let status = false
    for(const data of role){
      console.log(data.role_type,request.query.role)
      if(data.role_type == request.query.role){  
        status = request.token
        break
      } 
    }
    return status
   
  }
}