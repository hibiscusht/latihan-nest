/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common'
import { Observable } from 'rxjs'
import { RolesService } from './roles/roles.service'

@Injectable()
export class JwtGuard implements CanActivate {
  constructor(private roleService: RolesService){

  } 
  async canActivate(
    context: ExecutionContext,
  ):  Promise<boolean>  {
    const request = context.switchToHttp().getRequest()
    const role = await this.roleService.findAll()
    let status = false
    if(request.query.pass == 'yes'){
        request.token = 'abc1234'
        status = true
    }
    return status
   
  }
}